// 5 ggr till = "stop"

import java.util.Scanner;

public class Tamas1 {

	public static void main(String[] args) {

		String uppg = ""; // En variabel f�r att lagra f�rnamn, efternamn, och mobilnummer							// och mobilnummer


		String[] f_namn; // Tre array till att lagra f�rnamn, efternamn och mobilnummer i array
		f_namn = new String[5];

		String[] e_namn;
		e_namn = new String[5];

		String[] m_nr;
		m_nr = new String[5];

		Scanner data = new Scanner(System.in);

		int i2 = 0; // Variabel f�r att inneh�lla den aktuella radnummer i de tre array
					

		while (!uppg.equals("stop")) // Loop som g�r tills det blir "stop" i f�rnamn el. radnummer �r mindre en 5
										 
		{
			System.out.print("Ditt f�rnamn: ");
			uppg = data.nextLine();
			f_namn[i2] = uppg;
			
			if (uppg.equals("stop")) // Exit fr�n loop om uppg liknar med "stop"
				break;

			System.out.print("Ditt efternamn: ");
			uppg = data.nextLine();
			e_namn[i2] = uppg;

			System.out.print("Ditt mobilnummer: ");
			uppg = data.nextLine();
			m_nr[i2] = uppg;

			System.out.println("");

			i2++;

			if (i2 == 5) {
				break;
			}
		}

		System.out.println("");

		for (int i = 0; i < i2; i++) // Skriv bara de rader fr�n array som �r inte tomta 
		{
			System.out.println(f_namn[i] + " " + e_namn[i] + "\t"  + m_nr[i]);
		}

	}
}
